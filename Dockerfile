FROM python:3.9-slim

ARG ANTS_VERSION=master

LABEL maintainer="Flywheel <support@flywheel.io>"
ENV FLYWHEEL="/flywheel/v0"
RUN pip install poetry

# ANTs

# Borrowed from https://github.com/ANTsX/ANTs/blob/master/Dockerfile
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            software-properties-common \
            build-essential \
            apt-transport-https \
            ca-certificates \
            gnupg \
            software-properties-common \
            wget \
            ninja-build \
            git \
            zlib1g-dev \
            git \
            cmake && \
     rm -rf /var/lib/apt/lists/* \
     && mkdir -p /tmp/ants/build

RUN git clone https://github.com/ANTsX/ANTs.git /tmp/ants/source
RUN cd /tmp/ants/source && git checkout ${ANTS_VERSION} && cd ..

RUN cd /tmp/ants/build && \
    cmake \
        -DCMAKE_INSTALL_PREFIX=/opt/ants \
        -DBUILD_SHARED_LIBS=OFF \
        -DUSE_VTK=OFF \
        -DSuperBuild_ANTS_USE_GIT_PROTOCOL=OFF \
        -DBUILD_TESTING=OFF \
        -DRUN_LONG_TESTS=OFF \
        -DRUN_SHORT_TESTS=OFF \
        /tmp/ants/source 2>&1 | tee cmake.log && \
    make -j 4 2>&1 | tee build.log && \
    cd ./ANTS-build && \
    make install 2>&1 | tee install.log

# Move missing scripts to binary
RUN cd /tmp/ants/source/Scripts && \
    mv * /opt/ants/bin

ENV ANTSPATH=/opt/ants/bin/
ENV PATH=${ANTSPATH}:${PATH}

WORKDIR ${FLYWHEEL}
